// 登录平台为 B 端
export const enum Platform {
  OS = 1,
  B = 2,
}

/** 是否从别端跳b端 */
export const enum ISJUMPB {
  /** 是 */
  YES = 1,
  /** 否 */
  NO = 2,
}

/** 是否超管 */
export const enum SuperAdmin {
  /** 不是超管 */
  noSuper = 1,
  /** 超管 */
  isSuper = 2,
}
