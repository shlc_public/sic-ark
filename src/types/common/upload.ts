/** 上传图片的场景 */
export const enum UploadScene {
  /** 公共图片--（发布任务、） */
  PUBLIC_IMAGE = "PUBLIC_IMAGE",
  /** 身份证 */
  PERSON_ID_CARD = "PERSON_ID_CARD",
  /** 签约/发放模版 */
  EXCEL_TEMP = "EXCEL_TEMP",
  /** 接单签约管理--签约表*/
  TASK_ACCEPT_SIGN = "TASK_ACCEPT_SIGN",
  /** 发放管理--费用表*/
  TASK_PROJECT_DETAIL = "TASK_PROJECT_DETAIL",
  /** 费用管理--转账凭证*/
  TRANSACTION_PROVE = "TRANSACTION_PROVE",
  /** 发放明细--转账凭证*/
  TASK_PROJECT_DETAIL_RECEIPT_PDF = "TASK_PROJECT_DETAIL_RECEIPT_PDF",
  /** 合同管理--新增合同*/
  COMPANY_CONTRACT = "COMPANY_CONTRACT",
}
