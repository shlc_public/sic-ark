/** 任务费用 */
export const BillFeeTypeList = [
  { value: 1, label: "元/月" },
  { value: 2, label: "元/单" },
  { value: 3, label: "元/天" },
  { value: 4, label: "元/时" },
  { value: 5, label: "元/次" },
  { value: 6, label: "元/其他" },
  { value: 7, label: "%" },
];
