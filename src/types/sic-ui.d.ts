declare module "sic-ui" {
  const IconUI: any;
  const BackUI: any;
  const ButtonUI: any;
  const SwitchUI: any;
  const TextUI: any;
  const SearchUI: any;
  const TagUI: (props: any) => React.JSX.Element;
  const FormUI: any;
  const CellUI: any;
  const NotificationUI: any;
  const TableUI: any;
  const SelectUI: any;
  const AnchorUI: any;
  const DescribeUI: any;
  const MultiLineUI: any;
  const ModalUI: any;
  const ImageUI: any;
  const Tiga: (props: any) => React.JSX.Element;
}
