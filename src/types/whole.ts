// key-val键值对
export interface keyVal {
  [key: string]: string;
}

/** token失效 */
export const enum Response {
  /** 不存在 */
  ABSENT = "1000",
  /** 过期 */
  EXPIRE = "1004",
  /** 禁用 */
  DISABLE = "1006",
  /** 删除 */
  DELETE = "1007",
}
