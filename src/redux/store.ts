import { legacy_createStore as createStore } from "redux";
import allReducer from "./reducers";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage: storage,
};

const myPersistReducer = persistReducer(persistConfig, allReducer);

const store = createStore(myPersistReducer);
const persistor = persistStore(store);
export { store, persistor };
