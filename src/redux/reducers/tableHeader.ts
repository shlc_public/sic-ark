// 表头

export const defaultState = {
  tableList: [
    { type: "home", value: [] },
    { type: "personnelInformation", value: [] },
    { type: "task", value: [] },
    { type: "order", value: [] },
    { type: "releaseManagement", value: [] },
    { type: "releaseDetails", value: [] },
    { type: "costManagement", value: [] },
    { type: "rechargeApplication", value: [] },
    { type: "userManagement", value: [] },
    { type: "permissions", value: [] },
    { type: "releaseManagementDetails_success", value: [] },
    { type: "releaseManagementDetails_error", value: [] },
    { type: "releaseDraft", value: [] },
  ],
};

const changeTableHeader = (state = defaultState, action: { value: string; type: string }) => {
  switch (action.type) {
    case "changeTableHeader":
      return {
        tableList: action.value,
      };
    default:
      return state;
  }
};
export default changeTableHeader;
