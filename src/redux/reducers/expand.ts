// 侧边栏状态
const defaultState = {
  collapsed: false,
};
const changeExpand = (
  state = defaultState,
  action: { value: string; type: string }
) => {
  switch (action.type) {
    case "changeCollapsed":
      return {
        collapsed: action.value,
      };
    default:
      return state;
  }
};
export default changeExpand;
