import { combineReducers } from "redux";
import strictPage from "./strictPage";
import changeExpand from "./expand";
import changeUserInfo from "./userInfo";
import changeTableHeader from "./tableHeader";

const allReducer = combineReducers({
  strictPage,
  changeExpand,
  changeUserInfo,
  changeTableHeader,
});

export default allReducer;
