// 用户信息
const defaultState = {
  user: "",
};
const changeUserInfo = (
  state = defaultState,
  action: { value: string; type: string }
) => {
  switch (action.type) {
    case "userInfo":
      return {
        user: action.value,
      };
    default:
      return state;
  }
};
export default changeUserInfo;
