// 有权限的页面
const defaultState = {
  strictPageList: [],
};
const strictPage = (
  state = defaultState,
  action: { value: any; type: string }
) => {
  switch (action.type) {
    case "changePage":
      return {
        strictPageList: action.value,
      };
    case "clearPage":
      return {
        strictPageList: action.value,
      };
    default:
      return state;
  }
};
export default strictPage;
