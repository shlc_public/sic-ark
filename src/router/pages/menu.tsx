import { home } from "./home";
import { MenuInformation } from "@/types/common";

const menuInformation: MenuInformation[] = [];
menuInformation.push(...home);

export { menuInformation };
