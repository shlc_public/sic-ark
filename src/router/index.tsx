import { HashRouter, Routes, Route, Navigate } from "react-router-dom";

import BaseLayout from "@/layouts/base-layout/index";
import { menuInformation } from "./pages/menu";
import { connect } from "react-redux";
import { MenuInformation, ResourceList } from "@/types/common";
import React from "react";

const Router = () => {
  const newList = menuInformation;

  // 处理全局错误，可以记录错误信息
  window.onerror = function (message, source, lineno, colno, error) {
    console.error(message, source, lineno, colno, error);
    window.addEventListener("popstate", () => {
      location.reload();
    });
  };

  const renderResourcelist = (resourcelist: ResourceList[]) => {
    return (
      <>
        {resourcelist?.map((obj: ResourceList, index: number) => {
          if (obj?.key) {
            return <Route path={obj?.key} key={`${obj?.key}-${index}`} element={<obj.element />}></Route>;
          }
        })}
      </>
    );
  };
  const render = (menuList: any) => {
    return (
      <>
        {menuList?.map((item: MenuInformation) => {
          if (item?.children && item?.children?.length > 0) {
            return (
              <React.Fragment key={item.key}>
                {item?.resourcelist && renderResourcelist(item?.resourcelist)}
                <Route path={item?.key} key={item?.key} element={<item.element />}>
                  {render(item?.children)}
                </Route>
              </React.Fragment>
            );
          } else {
            return (
              <React.Fragment key={item.key}>
                <Route index element={<Navigate to={"/table"} replace />} />
                {item?.resourcelist && renderResourcelist(item?.resourcelist)}
                <Route path={item?.key} key={item?.key} element={<item.element limit={item?.resourcelist} />}></Route>;
              </React.Fragment>
            );
          }
        })}
      </>
    );
  };

  return (
    <HashRouter>
      <Routes>
        <Route path="/" key="/" element={<BaseLayout />}>
          {render(newList)}
        </Route>
        <Route path="*" key="*" element={<Navigate to={"/home"} replace />} />
      </Routes>
    </HashRouter>
  );
};

const ToState = (state: { strictPage: { strictPageList: any } }) => {
  return {
    menuList: state.strictPage.strictPageList,
  };
};
const ToDispatch = (dispatch: (arg0: { value: any; type: string }) => void) => {
  return {
    strictPage(value: any) {
      dispatch({ value, type: "changePage" });
    },
  };
};

const ConnectedComponent = connect(ToState, ToDispatch)(Router);
export default ConnectedComponent;
