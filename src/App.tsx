import Router from "@/router";
import { ConfigProvider, message } from "antd";
import zhCN from "antd/locale/zh_CN";
import "dayjs/locale/zh-cn";
import "@/style/global.scss";
import "@/style/sicAntd.scss";
import "@/style/publicClassName.scss";
import "@icon-park/react/styles/index.css";
function App() {
  message.config({
    duration: 3,
    maxCount: 3,
    rtl: false,
    prefixCls: "sicMessage",
  });

  return (
    <ConfigProvider locale={zhCN}>
      <Router />
    </ConfigProvider>
  );
}

export default App;
