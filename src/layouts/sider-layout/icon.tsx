import { HomeOutlined, SolutionOutlined, CopyOutlined, FormOutlined, AccountBookOutlined, SettingOutlined } from "@ant-design/icons";
import { MenuInformation } from "@/types/common";

export const menuIcon: MenuInformation[] = [
  { menuid: 1, icon: <HomeOutlined /> },
  { menuid: 2, icon: <SolutionOutlined /> },
  { menuid: 3, icon: <CopyOutlined /> },
  { menuid: 4, icon: <FormOutlined /> },
  { menuid: 5, icon: <AccountBookOutlined /> },
  { menuid: 6, icon: <SettingOutlined /> },
  { menuid: 7, icon: <SettingOutlined /> },
  { menuid: 8, icon: <SettingOutlined /> },
  { menuid: 9, icon: <SettingOutlined /> },
  { menuid: 10, icon: <SettingOutlined /> },
];
