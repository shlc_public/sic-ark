import { useState } from "react";
import { Layout, Menu } from "antd";
import { useNavigate, useLocation } from "react-router-dom";
import { connect } from "react-redux";
import { menuInformation } from "@/router/pages/menu";
import "./index.scss";

const { Sider } = Layout;

interface IProps {
  menuList: any;
  collapsed?: boolean;
}

const SiderLayout = (props: IProps) => {
  const { collapsed } = props;
  const navigate = useNavigate();
  const { pathname } = useLocation();
  //菜单点击项
  const [openKeys, setOpenKeys] = useState<string[]>();
  const a: any = menuInformation;

  const onOpenChange = (keys: string[]) => {
    const openRouter = keys?.[keys?.length - 1] ?? "/home";
    localStorage.setItem("router", JSON.stringify(openRouter));
    setOpenKeys([openRouter]);
  };

  return (
    <Sider
      collapsed={collapsed}
      width={212}
      className="siderlayout"
      style={{
        ["--bg_color" as string]: "#333",
      }}
    >
      <div className="logo">
        <div>Ark 方舟</div>
      </div>
      <div className="menu">
        <Menu
          theme="dark"
          mode="inline"
          key="menuid"
          selectedKeys={[pathname]}
          openKeys={openKeys}
          onClick={(item) => {
            if (item?.keyPath?.length === 1) {
              localStorage.setItem("router", JSON.stringify(item?.key));
              setOpenKeys([item?.key]);
            }
            navigate(item?.key);
          }}
          onOpenChange={onOpenChange}
          items={a}
        />
      </div>
    </Sider>
  );
};
const mapStateToProps = (state: { strictPage: { strictPageList: any }; changeExpand: { collapsed: boolean } }) => {
  return {
    menuList: state.strictPage.strictPageList,
    collapsed: state.changeExpand.collapsed,
  };
};
export default connect(mapStateToProps)(SiderLayout);
