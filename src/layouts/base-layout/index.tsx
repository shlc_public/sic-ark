import { Layout } from "antd";
import { Outlet } from "react-router-dom";
import SiderLayout from "../sider-layout/index";
import HeaderLayout from "../header-layout/index";
import "./index.scss";
const { Content } = Layout;

const BaseLayout = () => {
  return (
    <Layout className="layout">
      <SiderLayout />
      <Layout className="contenLayout">
        <HeaderLayout />
        <Content className="contenLayout_content">
          <Outlet></Outlet>
        </Content>
      </Layout>
    </Layout>
  );
};
export default BaseLayout;
