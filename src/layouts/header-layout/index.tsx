import { Layout, Button } from "antd";
import { connect } from "react-redux";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import "./index.scss";

const { Header } = Layout;

interface IProps {
  changeExpand: (value: boolean) => void;
  collapsed?: boolean;
}

const HeaderLayout = (props: IProps) => {
  const { changeExpand, collapsed } = props;

  return (
    <Header className="header_layout">
      <Button type="text" icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />} onClick={() => changeExpand(!collapsed)} />
      <div className="header_layout_right"></div>
    </Header>
  );
};

const ToState = (state: { changeExpand: { collapsed: boolean } }) => {
  return {
    collapsed: state.changeExpand.collapsed,
  };
};

const ToDispatch = (dispatch: (arg0: { value: boolean; type: string }) => void) => {
  return {
    changeExpand(value: boolean) {
      dispatch({ value, type: "changeCollapsed" });
    },
  };
};

const ConnectedComponent = connect(ToState, ToDispatch)(HeaderLayout);
export default ConnectedComponent;
