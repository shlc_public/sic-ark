import { useState } from "react";
import Center from "./components/center";
import Sider from "./components/sider";

const Home = () => {
  const initialTableData = [
    { key: "companyName", sort: 1, disable: true, width: 286, fixed: "left", name: "1" },
    { key: "zoneName", sort: 2, disable: false, width: 163, name: "2" },
  ];

  // 状态容器
  const [vessel, setVessel] = useState<any>({ tableHeader: initialTableData });

  return (
    <div className="p_pages">
      <Center vessel={vessel} setVessel={setVessel} />
      <Sider vessel={vessel} setVessel={setVessel} />
    </div>
  );
};

export default Home;
