import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import DraggableItem from "./components/draggableItem";
import { ButtonUI } from "sic-ui";
import { cloneDeep } from "lodash";
const Sider = (props: any) => {
  const { vessel, setVessel } = props;

  const handleDrop = (dragIndex: number, hoverIndex: number) => {
    const dragItem = vessel?.tableHeader[dragIndex];
    const newItems = cloneDeep(vessel?.tableHeader);
    newItems.splice(dragIndex, 1);
    newItems.splice(hoverIndex, 0, dragItem);
    newItems.forEach((item: any, index: number) => (item.sort = index + 1));
    setVessel({ ...vessel, tableHeader: newItems });
  };

  const addOne = () => {
    const newItems = cloneDeep(vessel?.tableHeader);
    const item = { key: `temp${newItems?.length + 1}`, sort: newItems?.length + 1 };
    newItems.push(item);
    setVessel({ ...vessel, tableHeader: newItems });
  };

  return (
    <div className="p_pages-sider">
      <div className="p_pages-sider-title">配置项</div>
      <DndProvider backend={HTML5Backend}>
        {vessel?.tableHeader?.map((item: any, index: number) => (
          <DraggableItem key={index} index={index} item={item} onDrop={handleDrop} />
        ))}
      </DndProvider>
      <div>
        <ButtonUI type="primary" onClick={addOne}>
          增加
        </ButtonUI>
      </div>
    </div>
  );
};
export default Sider;
