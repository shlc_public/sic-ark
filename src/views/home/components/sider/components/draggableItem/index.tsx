import { useEffect, useState } from "react";
import { Form, Input } from "antd";
import { TextUI } from "sic-ui";
import { useDrag, useDrop } from "react-dnd";
import { DownOutlined } from "@ant-design/icons";
import "./index.scss";

const DraggableItem = (props: any) => {
  const [form] = Form.useForm();
  const { index, onDrop, item } = props;
  const [showDetail, setShowDetail] = useState(false);

  useEffect(() => {
    form.setFieldsValue(item);
  }, []);

  const [, drag] = useDrag({
    type: "ITEM",
    item: { ...item, index },
  });

  const [, drop] = useDrop({
    accept: "ITEM",
    hover: (draggedItem: any) => {
      if (draggedItem.index !== index) {
        onDrop(draggedItem.index, index);
        draggedItem.index = index;
      }
    },
  });

  return (
    <div ref={(node) => drag(drop(node))} style={{ border: "1px solid", margin: "4px", padding: "8px" }} className="table-sider-pages">
      <Form form={form} className="form">
        <div className="simple">
          <div className="info">
            <div style={{ display: "flex" }}>
              <Form.Item name="key" label="字段">
                <Input style={{ width: 140 }} />
              </Form.Item>
            </div>
          </div>
          <div onClick={() => setShowDetail(!showDetail)}>
            <TextUI style={{ lineHeight: "32px" }}>
              <DownOutlined rotate={showDetail ? 180 : 0} />
            </TextUI>
          </div>
        </div>
        {showDetail && (
          <div>
            <Form.Item name="key" label="表头">
              <Input style={{ width: 140 }} />
            </Form.Item>
          </div>
        )}
      </Form>
    </div>
  );
};

export default DraggableItem;
