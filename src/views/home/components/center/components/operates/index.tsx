import { useState } from "react";
import { ButtonUI } from "sic-ui";
import OutModal from "../outModal";
import style from "./index.module.scss";
const Operates = (props: any) => {
  const { vessel } = props;
  const [showOutModal, setShowOutModal] = useState(false);

  return (
    <>
      <div className={style.operate}>
        <div className="title">表格组件快速生成</div>
        <ButtonUI onClick={() => setShowOutModal(true)}>导出配置</ButtonUI>
      </div>
      <OutModal showOutModal={showOutModal} setShowOutModal={setShowOutModal} data={vessel?.tableHeader} />
    </>
  );
};
export default Operates;
