import { ModalUI } from "sic-ui";
import { message } from "antd";
const OutModal = (props: any) => {
  const { showOutModal, setShowOutModal, data } = props;

  const confirm = (e: any) => {
    e.stopPropagation();
    try {
      navigator.clipboard.writeText(JSON.stringify(data, null, 2));
      message.success("文本已复制到剪贴板");
    } catch (err) {
      message.success("复制失败");
    }
  };
  return (
    <div>
      <ModalUI isOpen={showOutModal} setIsOpen={setShowOutModal} confirm={confirm} title={"配置详情"}>
        <div>
          使用说明：
          <br />
          1.导入sic-ui的Tiga（迪迦组件）
          <br />
          2.点击确认，使用此Json到指定文件位置
        </div>
        <div style={{ border: "1px solid", margin: "4px", padding: "8px" }}>
          <pre dangerouslySetInnerHTML={{ __html: JSON.stringify(data, null, 2) }}></pre>
        </div>
      </ModalUI>
    </div>
  );
};
export default OutModal;
