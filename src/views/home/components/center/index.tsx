import { Tiga } from "sic-ui";
import Operates from "./components/operates";
import tableData from "./data.json";

const Center = (props: any) => {
  const { vessel } = props;

  return (
    <div className="p_pages-center">
      <Operates vessel={vessel} />
      <div>
        <Tiga rowKey="globalDetailNo" tableHeader={vessel?.tableHeader} tableData={tableData} />
      </div>
    </div>
  );
};

export default Center;
